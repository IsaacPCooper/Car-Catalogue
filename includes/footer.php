<footer class="page-footer grey darken-2">
         <div class="container">
           <div class="row">
             <div class="left col l4 s12">
               <h5 class="white-text">Links to the developer's online content</h5>
               <ul>
                 <li><a class="grey-text text-lighten-3" href="https://github.com/JordanPCAustin">Github</a></li>
                 <li><a class="grey-text text-lighten-3" href="https://www.linkedin.com/in/jordan-austin-998845141/">LinkedIn</a></li>
                 <li><a class="grey-text text-lighten-3" href="https://jordanpcaustin.github.io/">Personal website of developer </a></li>
                 <li><a class="grey-text text-lighten-3" href="http://materializecss.com/">Framework Used: Materialize</a></li>
               </ul>
             </div>
           </div>
         </div>
         <div class="footer-copyright">
           <div class="container">
            2019 Jordan P C Austin
           <a class="grey-text text-lighten-4 right" href="#!"></a>
           </div>
         </div>
       </footer>
<!--Footer End-->
